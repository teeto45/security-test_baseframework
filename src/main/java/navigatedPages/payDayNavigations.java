package navigatedPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class payDayNavigations {

    public static final String LOGOUT_URL = "https://qa.seamlesspayday.com/login";

    public static final String BASE_URL = "https://qa.seamlesspayday.com";

    WebDriver driver;



    public payDayNavigations(WebDriver driver){

        this.driver = driver;

        this.driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);

        this.driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);

    }


    public void validLogin(String emailId, String password) {

        driver.get(BASE_URL);

        driver.findElement(By.xpath("//body//div[@id='app']//div//div//div//div[1]//input[1]")).sendKeys(emailId);

        driver.findElement(By.xpath("//div[2]//input[1]")).sendKeys(password);

        driver.findElement(By.xpath("//button[contains(text(),'Take me in')]")).click();
    }

//    public void manageCompany() {
//    }
}

/*The above code represents methods used to automate business functionalities in the test application,
like New User Registration, Navigate before Login, Login as User, Verify the presence of Text, and Navigate after logout.
Note: Similar methods can be created to automate other business functionalities in the application.*/
