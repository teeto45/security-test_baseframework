package driver;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class driverSetup {

    static WebDriver driver;

    public static WebDriver createChromeDriver(Proxy proxy, String path) {

        // Set proxy in the chrome browser
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability("proxy", proxy);

        // Set system property for chrome driver with the path
        System.setProperty("webdriver.chrome.driver", path);

        capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--ignore-certificate-errors");
        options.merge(capabilities);
        return new ChromeDriver(options);



    }


}


/*
The above code represents the creation and configuration of Chrome driver with proxy, driver path, and SSL certificates access.
Note: Similar methods can be created to configure other supported browsers like IE, Firefox, Safari, Edge, and others
*/

